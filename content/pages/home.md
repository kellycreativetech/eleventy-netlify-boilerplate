---
title: Home
date: 2016-01-01T00:00:00.000Z
permalink: /
layout: 'home.njk'
masthead:
  mastheadText: |-
      # This is a masthead title

      And here's a paragraph

      <p><a href="#test" class="button">call to actions</a></p>
  images:
    - image: 'https://ucarecdn.com/f958dd93-bc51-442c-bb81-60412663c5cd/'
    - image: 'https://ucarecdn.com/b2e6854e-629e-40d7-b3d4-86c72dccdde2/'
    - image: 'https://ucarecdn.com/b2e6854e-629e-40d7-b3d4-86c72dccdde2/'
    - image: 'https://ucarecdn.com/b2e6854e-629e-40d7-b3d4-86c72dccdde2/'
galleryImages:
  - image: 'https://ucarecdn.com/f958dd93-bc51-442c-bb81-60412663c5cd/'
    title: title 1
  - image: 'https://ucarecdn.com/b2e6854e-629e-40d7-b3d4-86c72dccdde2/'
    title: title 2
  - image: 'https://ucarecdn.com/77d690cf-f288-4006-adca-d2b6f6e6748e/'
    title: This is a testing image
  - image: 'https://ucarecdn.com/23d2e161-be02-4321-9ac8-6f22ee2c8684/'
    title: caption
---
This is a template for building a simple blog website with the [Eleventy static site generator](https://www.11ty.io), with deployment to [Netlify](https://www.netlify.com).

Includes [Netlify CMS](https://www.netlifycms.org) for WYSIWYG content editing, and [Netlify Forms](https://www.netlify.com/docs/form-handling) for processing your site's form data.
