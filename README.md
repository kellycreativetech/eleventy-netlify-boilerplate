### Kasey's notes

Here's what this includes:
* Eleventy static site generator
* Netlify CMS
* Hosted on Gitlab pages, with Cloudflare for HTTPS
* A Netlify-CMS-driven Navigation and Site Settings file, which populates an eleventy-formatted JSON file. This gives us a nice drag-and-drop two-tiered editing interface for site-wide navigation, as well as editable markdown fields for things like "address", which can be used in the footer with simple template tags. For example, `{{metadata.address | md }}`
* Uploadify for in-template image manipulation, CDN, and dropbox/google drive/etc image choosers
* in-page image gallery/portfolio example, with accurate frontend admin preview (as an example of both a basic eleventy and netlify-CMS list view, as well as bringing third-party resources into the CSS and JS pipelines)
* Masthead for homepage, which appears conditionally in the admin preview
* RSS/Podcast feed
* FormCarry integration for third-party form processing
* SCSS processing using the official `sass` npm package,
* CSS and JS cachebusting
* JS compiling with Rollup
* There's a branch with FullCalendar.io integration, using a CDN.

### TODO:
* mobile nav design and JS
* Pull Rollup.js out and use Eleventy's build system. Rollup is overkill for most of my projects.


### Run locally:

```
npm i
npm start
```


hat-tip to https://github.com/danurbanowicz/eleventy-netlify-boilerplate.git, which was where this project started.