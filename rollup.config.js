import commonjs from 'rollup-plugin-commonjs';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import resolve from 'rollup-plugin-node-resolve';
import url from 'rollup-plugin-url';

export default {
  input: './src/js/index.js',
  output: [
    {
      file: '_site/_includes/assets/js/bundle.js',
      format: 'iife',
      sourcemap: true
    },
  ],
  sourceMap: 'inline',
  plugins: [
    peerDepsExternal(),
    url(),
    resolve(),
    commonjs(),
  ]
}
