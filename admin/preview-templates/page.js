import htm from "https://unpkg.com/htm?module";

const html = htm.bind(h);

// Preview component for a Page
const Page = createClass({
  render() {
    const entry = this.props.entry;
    const galleryImgs = entry.getIn(["data", "galleryImages"]);
    const mastheads = entry.getIn(["data", "masthead"]);
    const mastheadLength = entry.getIn(["data", "masthead"]);
    const hasMasthead = entry.getIn(["data", "mastheadText"]);

    return html`
    <div className="masthead">
    ${mastheads && this.props.widgetsFor('masthead').getIn(['data', 'images']).map(
        im =>
          html`
            <img className="masthead-img" src="${im.getIn(['image']) + '-/scale_crop/1800x600/'}" />
          `)
      }
      <div className="masthead-text">
        ${this.props.widgetFor('masthead')}
      </div>
    </div>


      <main className="site-main l-container">
        <h1>${entry.getIn(["data", "title"], null)}</h1>

        ${this.props.widgetFor("body")}

        <ul className="thumbs">
        ${galleryImgs ?
          this.props.widgetsFor('galleryImages').map(
            img =>
              html`
                <li>
                <img src="${img.getIn(['data', 'image']) + '-/resize/200x/'}" />
                <p>${img.getIn(['data', 'title'])}</p>
                </li>
              `)
          : null}
        </ul>
      </main>
    `;
  }
});

export default Page;
