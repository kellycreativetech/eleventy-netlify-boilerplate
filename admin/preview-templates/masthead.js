var MastheadPreview = createClass({
  // Object fields are simpler than lists - instead of `widgetsFor` returning
  // an array of objects, it returns a single object. Accessing the shape of
  // that object is the same as the shape of objects returned for list fields:
  //
  // {
  //   data: { front_limit: 0, author: 'Chris' },
  //   widgets: { front_limit: (<WidgetComponent>), author: (WidgetComponent>)}
  // }
  render: function() {
    var entry = this.props.entry;
    var mastheadText = entry.getIn(['data', 'mastheadText']);
    var posts = entry.getIn(['data', 'posts']);

    return h('div', {},
      h('h1', {}, mastheadText),
      h('dl', {},
        h('dt', {}, 'Posts on Frontpage'),
        h('dd', {}, this.props.widgetsFor('posts').getIn(['widgets', 'front_limit']) || 0),

        h('dt', {}, 'Default Author'),
        h('dd', {}, this.props.widgetsFor('posts').getIn(['data', 'author']) || 'None'),
      )
    );
  }
});

CMS.registerPreviewTemplate("masthead", MastheadPreview);
